
\contentsline {chapter}{\numberline {1}Introduction}{6}{chapter.1}%
\contentsline {section}{\numberline {1.1}Property Based Testing}{6}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}PBT and formal verification}{7}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Generation of data}{7}{subsection.1.1.2}%
\contentsline {section}{\numberline {1.2}Project Details}{8}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Project Overview}{8}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Thesis Outline}{9}{subsection.1.2.2}%
\contentsline {chapter}{\numberline {2}PTB with QuickChick by example}{10}{chapter.2}%
\contentsline {section}{\numberline {2.1}Static and dynamic semantics of the Typed Arithmetic Expressions language}{10}{section.2.1}%
\contentsline {section}{\numberline {2.2}Testing progress}{12}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Test progress functionally }{13}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Generators}{15}{subsection.2.2.2}%
\contentsline {subsubsection}{Using automatic naive generators}{15}{section*.4}%
\contentsline {subsubsection}{Custom generators}{16}{section*.5}%
\contentsline {subsection}{\numberline {2.2.3}Deriving generators from Inductive definitions }{18}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}{Filtering a generator}}{19}{subsection.2.2.4}%
\contentsline {section}{\numberline {2.3}Mutation testing}{20}{section.2.3}%
\setcounter {tocdepth}{2}
\contentsline {chapter}{\numberline {3}The List Machine}{23}{chapter.3}%
\contentsline {section}{\numberline {3.1}Static and Dynamic Semantics}{23}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Machine syntax}{23}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Operational semantics}{25}{subsection.3.1.2}%
\contentsline {paragraph}{Sample program}{26}{section*.6}%
\contentsline {subsection}{\numberline {3.1.3}Type system}{26}{subsection.3.1.3}%
\contentsline {subsection}{\numberline {3.1.4}Instruction typing}{27}{subsection.3.1.4}%
\contentsline {subsection}{\numberline {3.1.5}Block typing}{28}{subsection.3.1.5}%
\contentsline {subsection}{\numberline {3.1.6}Program typing}{29}{subsection.3.1.6}%
\contentsline {section}{\numberline {3.2}Properties}{31}{section.3.2}%
\contentsline {section}{\numberline {3.3}Smart generators}{35}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Checking generators}{37}{subsection.3.3.1}%
\contentsline {section}{\numberline {3.4}Sampling generators}{37}{section.3.4}%
\contentsline {section}{\numberline {3.5}Mutations}{41}{section.3.5}%
\setcounter {tocdepth}{3}
\contentsline {chapter}{\numberline {4}Conclusion}{44}{chapter.4}%
\contentsline {section}{\numberline {4.1}Goals and results}{44}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}QuickChick vs FsCheck}{45}{subsection.4.1.1}%
\contentsline {section}{\numberline {4.2}Evaluation and future work}{45}{section.4.2}%
