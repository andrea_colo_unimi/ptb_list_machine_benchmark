\chapter{Introduction}
\section{Property Based Testing}
Software testing is a fundamental aspect of the software cycle:
however, it tends to be very expensive and can imply costs up to
50$\%$ of the cost of software development~\cite{Pezze}. Hence there
is an entire research area dedicated to its automation.

Let us just narrow the field to testing single units: they works by
specifying the behavior of a program by providing concrete examples of
it --- each test asserts how the program should behave against a
certain input~\cite{InPraise}.

A common issue in trying to automate this type of testing process is
the \emph{oracle problem}; once we have generated the inputs, who is in charge of 
determining the output of the program? %  Several testing approaches,
% including \emph{heuristic methods}, try to solve the oracle problem,
% but they do not always deliver exact
% solutions~\cite{huzar2011software}.

% What if there is a testing technique in which it is not necessary to determine the output of the program against a certain input?
 \emph{Property based testing}~\cite{claessen2011quickcheck} (PBT)  aims to bypass this issue.
Instead of listing the expected output against some inputs, the
programmer states a \emph{property} that should be always true for
every input of the program and encodes it as an executable Boolean code,
that given an input, outputs true or false depending on whether 
the property holds for the given input.
%
A testing library will then generate, typically in a pseudo-random way a large number of test cases trying to falsify the given property. If the latter stands, we can have some confidence that the code satisfies that specification.

One of the benefit of property based testing is that properties can be seen as both a way to test the code and
as specifications of the program. Writing specifications improves software design, for example documenting through properties what a software is supposed to do increases code reuse. 

Property based testing has been pioneered with the  QuickCheck  tool for Haskell~\cite{claessen2011quickcheck}, and versions of it are available for pretty much any programming language.

\subsection{PBT and formal verification}
A remarkable application of PBT is in the field of formal verification
via theorem provers or proof assistants. In fact theorem proving can
be a frustrating activity: in its fully automatic flavor, when a proof
fails, we may have no feedback at all. Conversely, if the proof has a
manual component as in the interactive case, failed proof attempts are
a labor intensive and not particularly efficient way to debug a
specification.

These are not novel observations, as this is exactly what
\emph{model-checking} aims to achieve. However, PBT can be integrated
much more smoothly with a proof assistant since it does not require a
specialized logic and in fact, PBT tools are now common in most
systems~\cite{BlanchetteBN11}, in order to discover that a statement is
false before trying to prove it; this aims to decrease the number of
failed proof attempts and consequently to reduce the cost of formal
verification.  In this thesis, we will use the \emph{Coq} system and
its PBT tool, QuickChick
\url{https://github.com/QuickChick/QuickChick}.

% In property based testing, usually, it is not easy to find the properties of a program and the programmer uses as a guideline several common 
% patterns in order to find properties, like those explained in ~\cite{Fun}.
% For example a common property that might be always true for a program is that  some things never change, like the contents of a collection after sorting it with a \lstinline|sort| function we want to test.~\cite{Fun}

% When property based testing is used as a form of \emph{validation} prior to theorem proving,
% we do not need to go through the above process in order to find properties; the properties that should hold are the theorems that we have to prove.~\cite{cheneyrun}.
%lascio un esempio commentato nel caso decidiamo di rimetterlo
%For example(from \emph{Software foundations} \citep{SF}) defining a remove function that given an x, removes every occurrence of x from the list.
%Note that there is an error: there is no recursive call in the branch \lstinline|if h =? x then t|. 
%\begin{lstlisting}
%Fixpoint remove (x : nat) (l : list nat) : list nat :=
%  match l with
%    | [] => []
%    | h::t => if h =? x then t else h :: remove x t
%  end.
%\end{lstlisting}
%And writing a property that I would like to prove: for every x, if I remove x from a list l of natural numbers, then x is no longer present in the list.
%\begin{lstlisting}
%Theorem removeP : forall x l,  ~ (In x (remove x l)).
%Admitted. 
%\end{lstlisting}
%QuickChick allows you to test the property by running the command:
%\lstinline|QuickCheck removeP.|
%The output shows that the property is false and as a counterexample
%finds x = 1 and l = [1; 1].
%Further details on the output returned by QuickChick are explained in Chapter 2.
%\begin{lstlisting}
%1
%[1; 1]
%*** Failed after 11 tests and 1 shrinks. (0 discards)
%\end{lstlisting}
%QuickChick was able to generate the inputs of the property, a natural number and a list of natural numbers,
%check that the theorem holds for all the inputs generated, and print the counterexample automatically,
%without further help from the programmer.
%In most cases the property will be rewritten using
%the appropriate combinators provided by QuickChick; furthermore QuickChick typeclasses will be used a lot.

Still, PBT is not a panacea and not a ``push-button'' technique as it
may sounds. As in other testing approaches, some care is needed,
specifically in the way random data is generated to avoid the usual
nightmare concerning \emph{coverage}.

\subsection{Generation of data}
Properties tends to be \emph{conditional} and testing must be aware of
that. In QuickChick they have the the form: \lstinline|<precondition> $==>$ <boolean predicate>|.  The $==>$ operator is not just  logical
implication but discards every input that does not respect the
precondition. If the latter is \emph{sparse}, that is unlikely to
hold, a positive response to our test does not mean much as the number
of tests effectively carried out is minimal. To fix ideas, consider
this Coq fragment that introduces a property for checking that
insertion in a list preserves sortedness and a call to the PBT tool
for validation. Note that in this thesis we assume familiarity with
Coq, in particular its syntax.

\begin{lstlisting}
Definition prop_insertOK (x:nat) (xs:list nat) :=
   sorted xs $==>$ sorted (insert x xs).
\end{lstlisting} 
\begin{lstlisting}
 QuickChecking prop_insertOK
*** Gave up! Passed only 4 tests
\end{lstlisting}
The 
bigger the list we generate randomly, the less likely it is that it happens to be sorted; hence the  unsatisfactory coverage.
% Once we have written an executable property, in order to test it, QuickChick requires a generator for the inputs of the property.
% A generator is implemented as a monad containing
% a random seed and a current maximum size.
% The size represents an upper bound on the depth of generated objects~\cite{SF}, but every generator can interpret the size in a different way, even ignoring it.
% During the testing process QuickChick continues to generate 
% random inputs using the random seed and tests them against the prop-------------------------------------erty, gradually increasing the size. The testing ends when either a counterexample is found or a certain number of test iterations is reached.
Most PBT libraries provides default generators for all common datatypes, for example \lstinline|FSCheck| for \lstinline|F$\#$|  can automatically derive generators  for algebraic data
types. However, for PBT to be effective, the user has to be cognizant
of the data distribution (to avoid false negatives, whereby
counterexamples are not found because the data distribution is skewed
in a way that is not compatible with the property and code at hand),
as well of the aforementioned coverage problem. The art of PBT lies
therefore in \emph{crafting} custom made generators.

% \subsubsection{The need for well-crafted custom generators}
% Property based testing is not just about writing a Boolean predicate,
% particular attention should be given to the data distribution of the generators.

% If random generation is used blindly without careful considerations about data distribution, two problems may occur: 
% \begin{enumerate}
% \item The generator is \emph{incomplete}: it could happen that a false property is true because the counterexample that makes the property false is never generated.
% For example consider the following false property that states that every natural number is less than $100$. (More about writing properties later)
% \begin{lstlisting}
% Definition every_nat_less_than_100 (x:nat) :=
%   x <? 99.
% QuickCheck every_nat_less_than_100.
% \end{lstlisting}
% QuickChick executes $500$ tests, each time testing a random generated natural number against the property; and unexpectedly the property succeeds.
% This happens because all the generated values are around $0$.
% \begin{lstlisting}[language=none]
% QuickChecking every_nat_less_than_100
% Passed 500 tests (0 discards)
% \end{lstlisting}
% \item The generator is \emph{unsound}: it can generate values that
%   do not respect the preconditions of the property.  Unsoundness can
%   lead to performance issues when testing properties with sparse
%   preconditions.  In these situations QuickChick continues to
%   generate random inputs but most of them are discarded since they
%   do not respect the preconditions; when a certain number of test
%   iterations has been reached, it gives up.


% Using automatic random generators QuickChick will not be able to test a property that states that after inserting an element, the list will still be sorted. This happens because most of the randomly generated lists are not sorted and after a very high number of test iterations QuickChick gives up.

% \end{enumerate}
% To mitigate the above problems, it is up to the programmer to use several combinators provided by QuickChick
% to create custom generators. Creating a custom generator, instead of using automatic random generators allows to have more control about data distribution, for example generating only inputs that satisfy certain preconditions.

% Alternatively, as we will briefly explore in this thesis, QuickChick includes a mechanism for automatically deriving generators for inductive relations along with proofs about their soundness and completeness with respect to the inductive relation they were derived from; for more information refer to~\citep{GG}.

A final ingredient is related to the notion of
$\delta$-debugging. During the testing process QuickCheck initially
generates small test cases and then gradually increases the size of
the generators; this means that when a failure case is found, it may
be rather complex to understand why it is a counterexample (think
about a very long list).  For this reason, QuickCheck tries to
minimize the counterexample to a minimal failing input; this process
is called \emph{shrinking}. Again, libraries offers default shrinkers,
but once one writes a custom generator, a custom shrinker is need as
well, and one that respects the invariants incorporated by the
generator. This is time-consuming and may introduce further errors in
counterexample reporting, but it is an essential part of effective
PBT. Having said that, in this thesis we do not address the issue of
custom shrinkers.

% For each input of the property of type \lstinline|A|, QuickChick calls a \lstinline|shrink| function that generates a sequence of elements of type \lstinline|A|, smaller than the given input.
% Then, all the elements of the list are tested against the property and
% if QuickChick finds another counterexample, the shrinking process is  repeated for this last one.
% %
% Keep going until it does not find another counterexample; the last counterexample found is the minimum counterexample. 
% ;

\section{Project Details}

\subsection{Project Overview}
%forse da migliorare nel caso ci penso di più
QuickChick, compared to  other QuickCheck clones, like FsCheck for F$\#$ and even to the property based testing features provided by other proof assistants like Isabelle~\cite{denes2014quickchick}, is still in a more experimental status.
%qui forse ho ripetuto qualcosa delle conclusioni
Anyway, it provides most of the features of QuickCheck and a few unique features like 
automatic derivation of generators from inductive predicates.

The goal of this thesis is to evaluate whether QuickChick is mature enough  to test complex programs and to evaluate the features provided of QuickChick compared to mainstream PBT tools.

First we  explore and review how property based testing in QuickChick works,
with a special attention on how to create a custom generator and to the automatic derivation features provided by QuickChick.

In order to do so, we  test several theorems of the Typed Arithmetic Expressions language presented in \emph {Software foundations}, chapter \emph {Type System}~\cite{SF2}. 

In this thesis I have reported only some examples; the whole project
 can be found at \url{https://bitbucket.org/andrea_colo_unimi/ptb_list_machine_benchmark/src/master/coq/}

\smallskip
Then we test several theorems of the  Coq implementation of the List Machine benchmark
\url{https://www.cs.princeton.edu/~appel/listmachine/}~\cite{LM} and we compare our work to the one by {Francesco Komauli}~\citep{komauli2018property} who tested the list-machine in F$\#$ using the library FsCheck.

Furthermore, we carry out some mutation analysis to evaluate out test suite.

Our work regarding the list-machine can be found at \url{https://bitbucket.org/andrea_colo_unimi/ptb_list_machine_benchmark/src/master/listmachine/ListmachineMapTree/}


% We provide here a short description of the list-machine to give an idea of how the list-machine is structured. \todo{spostare altrove}
%\subsubsection{The List-Machine Benchmark}
%\begin{addmargin}[5em]{1em}

% Several inductive predicates defines what it means for a program to run, the type-checking rules and the step relation.

% Among the theorems defined in the list-machine, we have tested using property based testing in QuickChick: \emph{progress}, \emph{preservation} and \emph{soundness}.
%\end{addmargin}


\subsection{Thesis Outline}
In this Chapter 1 I have sketched what property based testing is,
and why it can be useful in theorem proving.
The concepts of %sound and complete
generator and shrinking are also introduced.

Chapter 2 explains, through several examples, how
QuickChick works, including the typeclasses needed for property based testing and automatic derivation of generators.
Furthermore we explain how to manually insert mutations in a program using QuickChick.

Chapter 3 presents the list-machine benchmark~\citep{LM} including its static and dynamic semantics. We explain how we tested, using QuickChick, several theorems of the list-machine.

Chapter 4 concludes, comparing the features provided by QuickChick with
the ones provided by FsCheck, with special attention to the automatic
derivation capabilities of QuickChick and those of FsCheck. Finally we list some future work.





%  LocalWords:  QuickCheck FsCheck ScalaCheck PropEr Erlang monad GG
%  LocalWords:  QuickChick datatypes Francesco Komauli komauli LM
%  LocalWords:  typeclasses
