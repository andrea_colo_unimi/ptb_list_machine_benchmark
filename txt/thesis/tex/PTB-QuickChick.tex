\chapter{PTB with QuickChick by example}
In this chapter, I am going to touch upon some the main features of
QuickChick, while referring to the relevant chapter
in~\href{https://softwarefoundations.cis.upenn.edu/qc-current/index.html}{Software
  foundations} for more information.  We will concentrate on a toy
language, namely the Typed Arithmetic expressions language, and discuss how we
can setup a testing harness for the \emph{progress} property. We will
also mention in passing the related \emph{preservation} property.

\section{Static and dynamic semantics of the Typed Arithmetic
  Expressions language}
\label{sec:types}
We start with the syntax of the language, at the left as a BNF and at the right as a Coq \texttt{Type} :\mbox{}\\

\noindent\begin{minipage}{.45\columnwidth}
\begin{lstlisting}[language=none]
t ::= 
 true 
 | false
 | if t then t else t  
 | zero 
 | succ t 
 | pred t 
 | iszero t
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.45\columnwidth}
\begin{lstlisting}
Inductive tm : Type :=
  | ttrue : tm 
  | tfalse : tm
  | tif : tm -> tm -> tm -> tm 
  | tzero : tm
  | tsucc : tm -> tm   
  | tpred : tm -> tm   
  | tiszero : tm -> tm.  
\end{lstlisting}
\end{minipage}\hfill\\
Next the dynamic semantics, formulated as single-step relation:
\begin{lstlisting}
Inductive step : tm -> tm -> Prop :=
  | ST_IfTrue : forall t1 t2,  (tif ttrue t1 t2) $\longrightarrow$ t1
  | ST_IfFalse : forall t1 t2, (tif tfalse t1 t2) $\longrightarrow$ t2
  | ST_If : forall t1 t1' t2 t3, t1 $\longrightarrow$ t1' ->  (tif t1 t2 t3) $\longrightarrow$ (tif t1' t2 t3)
  | ST_Succ : forall t1 t1', t1 $\longrightarrow$ t1' -> (tsucc t1) $\longrightarrow$ (tsucc t1')
  | ST_PredZero : (tpred tzero) $\longrightarrow$ tzero
  | ST_PredSucc : forall t1, nvalue t1 -> (tpred (tsucc t1)) $\longrightarrow$ t1
  | ST_Pred : forall t1 t1', t1 $\longrightarrow$ t1' -> (tpred t1) $\longrightarrow$ (tpred t1')
  | ST_IszeroZero : (tiszero tzero) $\longrightarrow$ ttrue
  | ST_IszeroSucc : forall t1, nvalue t1 -> (tiszero (tsucc t1)) $\longrightarrow$ tfalse
  | ST_Iszero : forall t1 t1', t1 --> t1' -> (tiszero t1) $\longrightarrow$ (tiszero t1')
where "t1 '$\longrightarrow$' t2" := (step t1 t2).
\end{lstlisting}
%
Notice that the single-step relation can evaluate terms that do not make sense:
For example \lstinline|tsucc(tif ttrue ttrue ttrue)| can take a step, while it will eventually get stuck. To remedy this, we introduce a \emph{typing} discipline.

\smallskip
The types of a term of language include only natural numbers and booleans: \lstinline!T ::= Bool| Nat!
%
\begin{lstlisting}
Inductive typ : Type :=
  |TBool : typ
  |TNat : typ.   
\end{lstlisting}
We formulate the typing rules again as an \lstinline{Inductive} relation that associates terms to their types:
 \begin{lstlisting}
Inductive has_type : tm -> typ -> Prop :=
  | T_Tru : has_type ttrue TBool
  | T_Fls : has_type tfalse TBool
  | T_Test : forall t1 t2 t3 T,  
       has_type t1 TBool -> has_type t2 T -> has_type t3 T -> has_type (tif  t1 t2 t3) T 
  | T_Zro : has_type tzero TNat 
  | T_Scc : forall t1, has_type t1 TNat -> has_type (tsucc t1) TNat
  | T_Prd : forall t1, has_type t1 TNat -> has_type (tpred t1 ) TNat
  | T_Iszro : forall t1,  has_type t1 TNat -> has_type (tiszero t1) TBool.
   \end{lstlisting}
To ensure that the static and dynamic semantics are adequate, it is customary to prove two main properties:

\begin{itemize}
\item The progress statement: ``if a term is well typed, then either it is a value or it can take at least one step.''
\item And the preservation statement: ``When a well-typed term takes a step,
the result is also a well-typed term.''
\end{itemize}

Let us concentrate on progress. Its statement uses the
definition of the predicate \emph{value}; a term is a value if it is a
numeric value or a Boolean one.
 
 \begin{lstlisting} 
Inductive bvalue : tm -> Prop :=
  | bv_true : bvalue ttrue
  | bv_false : bvalue tfalse.

Inductive nvalue : tm -> Prop :=
  | nv_zero : nvalue tzero
  | nv_succ : forall t, nvalue t -> nvalue (tsucc t).

Definition value (t:tm) := bvalue t \/ nvalue t.

Theorem progress1 : forall t T, has_type t T ->  value t \/ exists t', t $\longrightarrow$ t'.
 \end{lstlisting} 

\section{Testing progress}
In order to test a property with \emph{QuickChick}, three items are required:
\begin{enumerate}
\item A way to automatically  generate (random) data;
\item A way to print counterexamples.
\item A way to define our property, here progress,  as an executable  predicate:
\end{enumerate}

Similarly to Haskell, Coq is a \emph{pure} functional programming
language, whereby we say that a function is pure if given an input it
always returns the same output without having any side effects, for
example altering a certain state. A random generator is not a pure function,
because every time it is sampled, the output may be different. A way to
describe computations that ``change the world'' in a pure setting is to
use the \emph{monadic} approach~\cite{wadler1995monads}.

In Coq, we use the \lstinline{Monad} typeclass:
\begin{lstlisting}
Class Monad (m : Type -> Type) : Type :=
       { 
         ret : forall t : Type, t -> m t;
         bind : forall t u : Type, m t -> (t -> m u) -> m u
       }.
\end{lstlisting}

A generator of elements of type \lstinline|A| has type
\lstinline|G A| where \lstinline|G| is an instance of the Monad typeclass.
Internally elements of \lstinline|G A| are functions that takes a size,
representing an upper bound on the depth of generated objects and a
random seed and return an \lstinline|A|.  When QuickChick samples a generator, it uses the random
seed to generate random elements, and progressively increases the size
until a counterexample is found or a certain number of tests is
reached.

QuickChick also provides the \lstinline|Show| typeclass
\lstinline|Class Show (A : Type) := {show : A -> string}|
representing all types \lstinline|A| such that we can define a function from \lstinline|A| to
string.  When a counterexample of type \lstinline|A|  is found, QuickChick searches
for the corresponding \lstinline{Show} instance, calls the \lstinline|show| method and
converts it to a string.
% 
Note that QuickChick can sometimes automatically derive
\lstinline{Show} instances for \lstinline{Inductive} type
declarations; for example, with the following command we have that for
the type \lstinline|tm|:
\begin{lstlisting}
Derive Show for tm.
\end{lstlisting}

Much more challenging is defining testable properties. What is
peculiar to Coq w.r.t.\ functional programming languages is that Coq
is also a \emph{logic}, and a constructive one. This means that there
are two realms of interest: computations, that live at type
\lstinline{Set} and deductions that live at
\lstinline{Prop}. Propositions may be arbitrary and possibly not even
decidable. However, if you can prove that the proposition is decidable
(it holds $A \vee \neg A$), then it can be tested.  To organize this
matter, QuickChick provides a typeclass \lstinline{Dec}, for
propositions for which it is possible to provide a proof of
decidability~\cite{GG}:
\lstinline|Class Dec (P:Prop) : Type := {dec:{P} + {~P}}.|

Since those proofs may not be easy to come up with and them maintain,
sometimes a simpler way to proceed is to restate relations that live
at \lstinline{Prop} as functions or Boolean predicates. Since Coq
permits only terminating functions, decidability is ensured.


\subsection{Test progress functionally }
% The definitions given previously have type Prop and are suitable to
% give a formal proof of progress, not for testing. 
Now I detail how
to rewrite the various relations detailed in Section~\ref{sec:types} in a functional way in order to
rewrite progress as an executable Boolean property.  The Boolean
predicates corresponding to \lstinline{bvalue} and \lstinline{nvalue} are as follows:

\noindent\begin{minipage}{.45\columnwidth}
\begin{lstlisting}
Fixpoint isnumericval (t:tm) : bool :=
    match t with
         tzero => true
        | tsucc t1 => isnumericval t1
        | _ => false
    end.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.45\columnwidth}
\begin{lstlisting}
Fixpoint isval (t:tm) : bool :=
    match t with
        | ttrue  => true
        | tfalse => true
        | t => isnumericval t
    end.
\end{lstlisting}
\end{minipage}\hfill

We re-write the type-checker (\lstinline{typeof}) as a \emph{partial}
function from terms and returning the corresponding type or \lstinline|None| if
the expression is ill-typed, i.e., the return type is \lstinline|option ty|.
Note that to determine the type of a term that is not a constant, for
example \lstinline|tsucc t1|, we have to obtain the type of
\lstinline|t1| using recursion; by
pattern matching on the result, if it is \lstinline|None| or it has a type
different from \lstinline|TNat| then return \lstinline|None|, else return \lstinline|TNat|.  The other
cases (\lstinline|tpred|, \lstinline|tiszero| and  \lstinline|tif|) similarly need to thread the result of a
recursive call making distinction on whether is it a \lstinline{None}
or a \lstinline{Some}. As popularized by Haskell, using the option
monad (called also the Maybe monad) makes the code more readable and
concise.
\begin{lstlisting} 
Instance optionMonad : Monad option := {
    ret T x :=
      Some x ;
    bind T U m f :=
      match m with
        None => None
      | Some x => f x
      end }.
\end{lstlisting}
Remember that defining a monad instance requires us to provide implementations for the
return and bind operation and we have done so following the usual
encoding of partiality.  To determine the type of a constant just
write \lstinline|ret| followed by the type of the constant, for example
\lstinline|ret TBool|.  To determine  the type of a term
that is not a constant, for example \lstinline|tsucc t1|, use as the
first argument of bind the result of \lstinline|typeof t1|; if it is
\lstinline|None|, the result of bind is \lstinline|None|, else \lstinline|t'| can be used in the rest of
the computation. %, a function with return type \lstinline|option typ|.
%ret is a function that
% given a x of a certain type T returns Some x:
%  \begin{lstlisting} 
% ret := fun (T : Type) (x : T) => Some x
%  \end{lstlisting} 
% and bind is a function that takes an input m of type option T,
% and a function f from T to another option type,
% if m is not None then returns f x.
% \begin{lstlisting} 
% bind := fun (T U : Type) (m : option T)
%           (f : T -> option U) =>
%         match m with
%         | Some x => f x
%         | None => None
%         end|.
% \end{lstlisting} 
Coq provides the usual monadic let notation \lstinline|x <- m1 ;; m2.| 
instead of writing \lstinline| bind m1 (fun x => m2)|.
The complete listing of the type-checker follows:


\begin{minipage}{.45\columnwidth}
\begin{lstlisting}
Fixpoint typeof (t:tm) : option typ :=
  match t with
  | ttrue => ret TBool
  | tfalse => ret TBool
  |  tzero => ret TNat
  | tsucc(t1) =>
      t' <- typeof t1 ;;
      if eqTy t' TNat then ret TNat
      else None
  | tpred(t1) =>
      t' <- typeof t1 ;;
      if eqTy t' TNat then ret TNat
      else None
  | tiszero(t1) =>
      t' <- typeof t1 ;;
      if eqTy t' TNat then ret TBool
      else None
  | tif t1 t2 t3 =>
      t' <- typeof t1 ;;
      if eqTy t' TBool then
        t' <- typeof t2 ;;
        t'' <- typeof t3 ;;
        if eqTy t' t'' then ret t'
        else None
      else None
  end.
\end{lstlisting}
\end{minipage}
\begin{minipage}{.45\columnwidth}
\begin{lstlisting}
Definition eqTy (t1 t2 : typ) : bool:=
  match t1,t2 with
  |TBool,TBool => true
  |TNat,TNat => true
  |_,_ => false
 end.
 
Definition typeable (e:tm) : bool :=
  match typeof e with
  |Some _ => true
  |_ => false
  end.
\end{lstlisting}
\end{minipage}\hfill

Using \lstinline|typeof| we have defined the Boolean predicate
\lstinline{typeable}: a term has a type if the result of \lstinline|typeof| is
different from \lstinline|None|. Following Peirce's book~\cite{pierce2002types}, we
have chosen to encode (object) type equality (\lstinline{eqTy})
explicitly, instead of relying on Coq's equality. This does not make
any difference here, but it is better for generalization to more
interesting type equalities such as the one induced by recursive types.

Finally \lstinline{stepF} is the functional version of the step relation, making
one reduction step according to the same rules of \lstinline|step|. The output of
\lstinline{stepF} is \lstinline|option tm| because some terms cannot make a step, for example
\lstinline|ttrue|; so for the same reasons of \lstinline|typeof|, it is useful to use the
option monad.
\begin{lstlisting}
Fixpoint stepF (t:tm) : option tm  :=
   match t with
      | tif ttrue t2 t3 => ret t2
      | tif tfalse t2 t3 => ret t3
      | tif t1 t2 t3 =>
           t1' <- stepF t1 ;;
           ret (tif t1' t2 t3)
      |tsucc(t1) =>
           t1' <- stepF t1 ;;
           ret (tsucc(t1'))
      |tpred  tzero => ret  tzero
      |tpred(tsucc(nv1)) =>
          if (isnumericval nv1) then ret nv1
          else
            t1' <- stepF nv1 ;;
            ret (tpred(tsucc(t1')))
      |tpred(t1) =>
           t1' <- stepF t1 ;;
           ret (tpred(t1'))
      |tiszero  tzero => ret ttrue
      |tiszero(tsucc(nv1)) =>
          if (isnumericval nv1) then ret tfalse
          else
           t1' <- stepF nv1 ;;
           ret (tiszero(tsucc( t1')))
      | tiszero(t1)  =>
           t1' <- stepF t1 ;;
           ret (tiszero( t1'))
      |_ =>  None
  end.
\end{lstlisting}
Using \lstinline|stepF| we can define the \lstinline|canStep|
predicate, which holds only if a term can make a step.
\begin{lstlisting}
Definition canStep (e:tm)  : bool :=
    match stepF e with
    | Some _ => true
    | None => false
    end.
\end{lstlisting}
%\newpage

\subsection{Generators}
So far, I have obtained a functional version of the predicates used by
the progress theorem; now I define generators for the inputs to the
property. 

In the following I show two approaches to define a
generator: either use QuickChick to automatically obtain a generator
for terms or write a custom generator.
\subsubsection{Using automatic naive generators}
Just like for the \lstinline|Show| instance, QuickChick can automatically derive a generator for Inductive types with no arguments:
\begin{lstlisting}
Derive Arbitrary for tm.
\end{lstlisting}
In the above code, \lstinline|arbitrary| is a method of the Gen typeclass, which
provides a generator for automatically derived types, in this example
\lstinline|tm| and a name to refer to
that generically:
\begin{lstlisting}
Class Gen (A : Type) := {arbitrary : G A}.
\end{lstlisting}


%The above code declares \lstinline|tm| as an %instance of the \lstinline|Arbitrary| typeclass, %representing all types that can be randomly %generated or shrunk:
%\begin{lstlisting}
%Class Arbitrary (A : Type) `{Gen A} `{Shrink A}
%\end{lstlisting}

Now we are almost ready to test progress: the only lacking ingredient
is a way to combine preconditions, namely here the typability of a
term, with a post-condition, i.e., satisfying the progress property. As
in Haskell's QuickCheck there is the combinator \verb|==>|, which,
roughly, takes two properties (called ``checkers'' in QuickChick's lingo), and tests that if the precondition
holds, the post-condition succeeds, discarding all terms that do not
satisfy the precondition~\cite{paraskevopoulou2014quickchick}.  In the
following case, we aim to generate terms, discarding all the ill-typed
ones, until we find a counterexample or
exhaust a given limit (\lstinline{MaxFail}) for terms not satisfying
the precondition.

\begin{lstlisting}
Definition progressP1 (e:tm) :=
   (typeable e) $\Longrightarrow$ (isval e || canStep e).
 
QuickCheck progressP1.
Passed 500 tests (490 discards)
\end{lstlisting}
QuickChick manages to test directly progress, but it is not plain
sailing. In fact, as the output shows, in this run \lstinline|490|
terms were discarded, while generating terms that satisfy the
precondition and the progress conclusion. The sparser the
precondition, the worse coverage gets. In order to do better, we have
to write our own custom generator.

\subsubsection{Custom generators}
When writing a custom generator for a \emph{recursive} type, it is
necessary to parameterize it with a size, which will be decreased in
recursive calls: this will satisfy Coq's termination check, but it
also makes sense for functional languages, since random choices will
lead to non-termination and so to a not very useful generator.

For example, the recursive custom generator \texttt{gen\_term\_size} depicted in Figure~\ref{fig:gent} takes as
inputs a size and a type \lstinline|t| and generates only terms that have type \lstinline|t|, according to the following strategy:

If the size is zero we generate a terminal term
by pattern matching on the type \lstinline|t| and using QuickChick's combinators~\citep{SF}:
\begin{itemize}
\item \lstinline|returnGen|, which takes a constant and yields a generator that always returns this value;
\item \lstinline|oneOf|, which  takes a list of generators, and picks one of the generators from the list uniformly at random.
\end{itemize}

Otherwise use the \lstinline|choose| combinator to generate the next
size that will be less than the current size.  Always using pattern
matching on the type generate either a terminal term or recursively
generate terms and then combine the results using the
\lstinline|liftGen| or the \lstinline|liftGen3| combinators, which
lift a function over a generator, depending on the arity of the
former.
% \begin{itemize}
% \item liftGen takes as input a function \verb|(A -> B)| and applies it to the result of a generator \verb|G A|
% \item liftGen3 takes a function of 3 arguments, and applies it to the result of 3 generators.
% \end{itemize}
Finally, we package in \texttt{gen\_term} the sized combinator
that takes a generator parameterized by a size, \lstinline|gen_term_size| and
produces an unsized one, so that QuickChick may increase size during checking.


 \begin{figure}[t]
   \centering
   \begin{lstlisting}
Fixpoint gen_term_size (n:nat) (t:typ) : G tm :=
  match n with
    | 0 =>
      match t with
      |TNat => returnGen  tzero
      |TBool => oneOf [returnGen ttrue; returnGen tfalse]
      end
    | S n' =>
        do! m <- choose (0, n');
        match t with
        | TNat =>
          oneOf [returnGen  tzero;
                 liftGen tsucc (gen_term_size (n'-m) TNat) ;
                 liftGen tpred (gen_term_size (n'-m) TNat) ;
                 liftGen3 tif (gen_term_size (n'-m) TBool)
                               (gen_term_size (n'-m) TNat)
                               (gen_term_size (n'-m) TNat)]
        | TBool =>
          oneOf [returnGen ttrue; returnGen tfalse;
                liftGen tiszero (gen_term_size (n'-m) TNat);
                liftGen3 tif (gen_term_size (n'-m) TBool)
                               (gen_term_size (n'-m) TBool)
                               (gen_term_size (n'-m) TBool)]
        end
    end.

Definition gen_term (tau : typ) :=
  sized (fun s => gen_term_size s tau ).
\end{lstlisting}

\caption{Random generator for well typed terms}
\label{fig:gent}
\end{figure}

To write a property that refers to custom generators we use the combinator
\lstinline{forAll}.  As the name suggests, this encodes universal
quantification over a given generator. Hence, it takes a generator of
type \lstinline|G A| and a function that uses the elements generated
in the rest of the property.
\begin{lstlisting}
Derive Arbitrary for typ.
\end{lstlisting}
Here we have resorted to QuickChick's automatic derivation of generators for object-level types. Now for the query, which uses two nested generators:

\begin{lstlisting}
Definition progressP2 :=
  forAll arbitrary (
              fun tau =>
               forAll (gen_term tau)
               (fun t =>
                (isval t || canStep t).          
               )).
 \end{lstlisting}                
For each type \lstinline|tau| generated by the generator
\lstinline|arbitrary| of type \lstinline|G typ|, every (well-typed)
term of type \lstinline|tau| generated using the generator
\lstinline|gen_term| is either a value or can take a step.

 QuickChick validates the property and there are $0$ discards.
 
 \subsection{Deriving generators from Inductive definitions }
 
QuickChick is (sometimes) able to automatically derive a \emph{constrained}
 generator~\cite{GG}, i.e.\ one that generates terms that satisfy a predicate
 defined as \lstinline|Inductive|. %  Since it is not guaranteed that a term that
 % satisfies the predicate exists, these generators are
 % partial\cite{SF}. 
 For example, another way to generate only well-typed terms boils down to the following command:
\begin{lstlisting}
Derive ArbitrarySizedSuchThat for (fun t => has_type t tau).
  \end{lstlisting}

\lstinline|ArbitrarySizedSuchThat| generates an instance of the class:

\begin{lstlisting}
Class GenSizedSuchThat (A : Type) (P : A $\rightarrow$ Prop) :=
{
     arbitrarySizeST : nat $\rightarrow$ G (option A)
}. \end{lstlisting}
Since it is not guaranteed that any such \lstinline|A| exists, these generators are partial.
This instance is automatically converted to an instance of \lstinline|GenSuchThat|, which provides
a method \lstinline|arbitraryST|.
\begin{lstlisting}    
Class GenSuchThat (A : Type) (P : A $\rightarrow$ Prop) :=
{
     arbitraryST : G (option A)
}
\end{lstlisting}
Now we obtain a generator for well-typed terms:
\begin{lstlisting}
@arbitraryST _ (fun t => has_type t tau) _ 
\end{lstlisting}
or using the \lstinline|genST| notation:
\begin{lstlisting}
Notation "'genST' f" := (@arbitraryST _ f _) 

genST (fun t => has_type t tau)
\end{lstlisting}

Thus, we can define the progress property with a generator directly
based on the rule-based definition of typing, rather than with a
detour via a functional encoding:

\begin{lstlisting}
Definition progressST :=
  forAll arbitrary (fun tau : typ =>
          forAll (genST (fun t => has_type t tau))
               (fun mt =>
                   match mt with
                     | Some t =>  (isval t || canStep t)
                      | None => false (* does not happen *)
                      end)).
\end{lstlisting}
Note that the partiality here plays no role, since in this case all objects type are inhabited and we can always construct such well-typed terms.

% Some limitations of deriving generators using this approach are that
% currently, QuickChick does not support automatic derivation of generators from the conjunction or disjunction of Inductive predicates.
However, automatically deriving generators from inductive definitions
is still a research topic~\cite{GG} with all sorts of (non-documented) limitations -- for example it does not apply to dependent types. More disconcertingly 
sometimes it fails in cases that should not be problematic.  For example consider deriving a generator for \emph{sorted} lists of numerals~\footnote{\url{https://github.com/QuickChick/QuickChick/issues/196\#issuecomment-615347556}}.
\begin{lstlisting}
Inductive isorted : list nat -> Prop :=
  | inil: isorted nil
  | iss: forall x, isorted [x] 
  | ic: forall x y l, isorted (y :: l) -> le x y
     -> isorted (x :: y :: l).

Derive ArbitrarySizedSuchThat for (fun t => isorted t). (* FAILS *)
\end{lstlisting}
%\newpage\cleardoublepage

\subsection{{Filtering a generator}}
Consider testing the other crucial property of a type system w.r.t.\ its operational semantics, \emph{preservation}:
``When a well-typed term takes a step,
the result is also a well-typed term'', or as a rule:
$$
\infer{\Gamma \vdash  t' : T } { 
\Gamma\vdash t: T 
&
 t \rightarrow t' }
$$

Using the functional specification we can readily encode this:
\begin{lstlisting}
Definition preservation e :=
  match typeof e with
     | None => true
     | Some tau => 
      match stepF e with
      |None => true 
      |Some e' =>
        (typeof e'= Some tau)?
      end
  end.
\end{lstlisting}
However, testing this takes awhile and it is very wasteful, even with
such a trivial type system.  One possibility would be to try to derive
a generator via \lstinline{ArbitrarySizedSuchThat} over the
conjunction of the premises, namely well-typed terms that can make a
step. Currently, QuickChick does not support automatic derivation of
generators from the combination of Inductive predicates.  To avoid
writing a custom generator (the more complex a generator is, the more
is prone to errors), we can \emph{filter} the generator of well-typed
terms, \lstinline|gen_term|, with a Boolean predicate using the
\lstinline{suchThatMaybe} combinator.  Given a generator and a
predicate, \lstinline|suchThatMaybe| generates a value that satisfy
this predicate or \lstinline|None| if it fails to find one.

\begin{lstlisting}
suchThatMaybe  : G A -> (A -> bool) -> G (option A)

Definition wellTypedTermThatCanStepGen T :  G (option tm) :=
  suchThatMaybe (gen_term T) canStep.
\end{lstlisting}


Now, we can formulate preservation as a variation of our version of progress, via a modified \lstinline|preservation| predicate where the type is input:
\begin{lstlisting}
Definition preservationP : :=
   forAll arbitrary (
               fun tau =>
                 forAll (wellTypedTermThatCanStepGen tau)
                 (fun e =>
                    match e with
                    |None => true
                    |Some e' => preservationT e' tau
                    end
                 )).

Definition preservationT (e:tm) (tau:typ) :=
   match stepF e with
   |None => true 
   |Some e' =>
     match typeof e' with
     |Some tt' => (tt' = tau)?
     |None => false
     end
    end.
\end{lstlisting}
                 % The predicate \lstinline|preservation| can be read in this way:
% if a term \lstinline|t| can not take a step,
% then the preservation hypotheses are not respected, therefore the property is true.
% Otherwise take a step from \lstinline|t|  to \lstinline|t'| and check that the type of \lstinline|t'| is equal to \lstinline|tau|, the type of \lstinline|t|, passed as input.
I will not report further details about preservation; please consult the repo.
%\newpage

\section{Mutation testing}
To evaluate the quality of a testing suite to detect errors, it is
customary to use \emph{mutation testing}~\cite{papadakis2019mutation}.
Mutation testing, with regard to PTB, can be divided into three
phases:
\begin{enumerate}
\item First introduce a single fault into the program according to certain mutation \emph{operators}, creating different versions of the program.
Each mutated version is called a mutant.
\item Test the properties for each mutant; if at least a property is
  falsified, then we say that the mutant is \emph{killed} by this
  property. %The ratio between the number of mutants and of those killed is called the \emph{mutation score}
  The ratio between the number of mutants killed and the total number
  of (non-equivalent) mutants is called the \emph{mutation score}. The
  higher the score, the better the testing suite, here the properties
  under test and the related generators.
\item If possible, refine the test suite to improve the mutation score.
\end{enumerate}

% There is in a recently a tool that can automatically generate
% mutants for Coq~\cite{celik2019mutation}, but it is not connected to
% QuickChick yet.
QuickChick offers some commands to facilitate the
checking of manually crafted mutation.  
%
A mutation in QuickChick is basically commented code that will be replaced and checked at run time. It consists of 3 parts~\cite{SF}:
\begin{enumerate}
\item \lstinline|(*! *)| indicates the beginning of the mutation and it is followed by the correct code.
\item \lstinline|(*!! *)| is optional and gives a name to the mutant.
\item Then we write between \lstinline|(*| and \lstinline|*)|
a mutation that will replace the correct code.
\end{enumerate}

For example the following excerpt of the definition of \lstinline|has_type|, where
others constructors have been elided, contains a mutation for the \lstinline|T_Scc|
rule. This comes from an exercise in the \emph{Software foundations}
course\footnote{\url{https://softwarefoundations.cis.upenn.edu/plf-current/Types.html}}:


\begin{lstlisting}
Inductive has_type : tm -> typ -> Prop :=
...
(*! *)
  | T_Scc : forall t1,
        has_type t1 TNat ->
        has_type (tsucc t1) TNat
  (*!! test-bug *)
  (*! 
  | T_SccBool : forall t,
           has_type t TBool ->
           has_type (tsucc t) TBool
 *)
  ...
\end{lstlisting}

The command
\lstinline|quickChick -color -top T -tag test-bug|
%
runs QuickChick on this mutant alone\footnote{See Software
  Foundations volume in QC~\cite{SF} for more information about command-line
  options.}. The progress property  \lstinline|progressST| kills this mutant, and
QuickChick shows the type and term generated that falsify the
property. 
\begin{lstlisting}[]
TBool
Some tif (tsucc tfalse) (tiszero (tif (tsucc ttrue) (tpred tzero)
           (tif ttrue tzero tzero))) tfalse
Checking Progress.progressST...
*** Failed after 40 tests and 0 shrinks. (0 discards)

\end{lstlisting}
As the reader can see, the counterexample is unnecessarily large, while \lstinline|(tsucc tfalse)| would have sufficed. Ideally,
as explained in the first chapter, when QuickChick finds a failure
case, it will try to reduce it to a minimal counterexample,
(\emph{shrinking}).  In order for this process to work, there must be
a shrinking function. For automatically derived generators, this is
provided by the system, while for custom ones it is the user's
responsibility. However, this is less trivial than it seems, as shrinking must be  careful to
respect the relations between the involved terms. For example, considering the above
counterexample, if we shrink the second branch without considering that
the result of this shrinking process must be of the same type of the
first input, the result might be incorrect.


This thesis is focused on
generators and we have not implemented custom shrinkers; when a shrinker is
\emph{not} implemented, the output  \lstinline|0 shrinks| signals that.
%\newpage

%  LocalWords:  PTB QuickChick BNF booleans Bool typeclass Monad TNat
%  LocalWords:  bvalue nvalue typeof typeable tpred tiszero tif monad
%  LocalWords:  ret typ stepF tm ttrue canStep returnGen oneOf forAll
%  LocalWords:  liftGen arbitraryST genST isorted suchThatMaybe Scc
%  LocalWords:  ArbitrarySizedSuchThat decidability Peirce's arity

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End:
%  LocalWords:  typability QuickCheck combinators unsized repo
