\documentclass[12pt,a4paper]{article}
\usepackage{tocvsec2}
\setcounter{tocdepth}{3}
\usepackage{geometry}
%\geometry{letterpaper,margin=1in}

\newcommand{\summary}[3]{%
  \section*{%
    \parbox{\textwidth}{%
      \centering
      #1\\[1.5ex]% the title and the separation
      \normalfont\large
      #2\\[1.5ex]% 
      \large Relatore: Professor Alberto Momigliano
      #3\\[2ex]% the author and the separation
      \large  Termine tirocinio 30/04/2020
    }%
  }%
  %\addcontentsline{toc}{chapter}{Summary}
}

\usepackage{listings,todonotes}
\usepackage{proof,url}
\usepackage[utf8]{inputenc}

 \usepackage{natbib}

\usepackage{xcolor}

\usepackage{fullpage}

\usepackage{hyperref}
\usepackage{xspace}

\usepackage{amssymb,amsmath,amsthm}
\usepackage{thesis}

\title{Property-Based Testing of the List-machine Benchmark with QuickChick}
\author{Andrea \sc{col\`{o}}}
\dept{Corso di Laurea Triennale in Informatica}
\anno{2019-2020}
\matricola{893093}
\relatore{Professor Alberto \sc{Momigliano}}



 \input{lstcoq.sty}
 \lstset{language=coq}
 \lstset{basicstyle=\footnotesize}

\lstdefinelanguage{none}{
  identifierstyle=,
  keywordstyle=
}



\begin{document}


\summary{Property-Based Testing of the List-machine Benchmark with QuickChick}
        {ANDREA COLO'– 893093}
        {}


\section{Introduction}
Software testing is a fundamental aspect of the software cycle:
however, it tends to be very expensive and can makes up to
50$\%$ of the cost of software development~\cite{Pezze}. Hence there is an entire research area dedicated to its automation.

An approach to partially automate some aspects of the testing process is reasoning in
terms of the properties that must be always true for every input of
the program instead of listing the inputs and the expected
output. This technique is called \emph{property based testing} (PBT) and it is the
main topic of this thesis.

In PBT, the programmer states a property that the code should
obey and encodes it into an executable predicate, which is then tested
over a large set of inputs, typically in a random way.

Still, PBT is not a panacea and not a ``push-button'' technique as it
may sounds. As in other testing approaches, some care is needed,
specifically in the way random data is generated to avoid false negatives, and inefficiencies.

A remarkable application of PBT is in the field of formal verification
via theorem proving.  In fact, theorem proving is labor intensive and
can be a frustrating activity when it fails, since failed proof
attempts are not a particularly efficient way to debug a
specification.

Before proving a theorem, therefore the programmer/verifier may be advised to
rewrite it as an executable property and tests it on a large set of
inputs with the intention of checking the existence of a counterexample, thus decreasing
the number of failed proof attempts and consequently reducing the cost
of formal verification.

This is the main goal of QuickChick~\cite{denes2014quickchick}, a clone of Haskell’s QuickCheck~\cite{claessen2011quickcheck} for Coq \url{https://coq.inria.fr}.
It provides most of the functionalities of QuickCheck and a few unique features like automatic derivation of generators from inductive predicates.

\section{Project overview and results}
QuickChick, compared to other QuickCheck clones, and in particular  the
property based testing features provided by other proof assistants
like Isabelle, is still in a more experimental
status.

The goal of this thesis is to evaluate whether QuickChick is mature
enough to test complex programs and to compare its features to the
ones provided by common mainstream PBT tools.

%In this thesis we expect familiarity with Coq 
%I learned it during this intership

First we explore and review how property based testing in QuickChick
works, with a special attention on how to create a custom generator
and to the automatic derivation features provided by QuickChick.

In order to do so, we have tested several theorems of the Typed
Arithmetic Expressions language presented in \emph {Software
  foundations}, chapter \emph {Type System}~\cite{SF}.

We provide here a summary of our methodology.  First the programmer
rewrites the inductive predicates used in the theorems in a functional
way and writes an executable property using several QuickChick
combinators.  A property in order to be executed requires a generator
and a printer; both of them can be automatically derived for the types
of the Typed Arithmetic Expressions language.  Since type soundness is
characterized by sparse preconditions, automatically derived random
generators are not suitable.  We have investigated three modalities:
\begin{itemize}
\item We can write a custom generator so that every generated value
  respect the preconditions of the property.

\item We can filter the automatically derived random generators with a
  Boolean predicate in order to obtain
  a generator that satisfies the preconditions of the properties.

\item We can automatically derive a generator from the inductive
  predicates of the Typed Arithmetic Expressions language.  This
  feature is very promising since instead of writing a custom
  generator we can automatically derive a generator whose generated
  inputs satisfy an inductive predicate in just one line of code.
  However, note that automatically deriving a generator from an
  inductive predicate is still a research topic~\citep{GG} and as we
  have explained in the conclusion, in certain situations it does not
  work.
\end{itemize}

\smallskip Then we have applied these techniques to validate several
theorems of a more complex case study, the list-machine
benchmark~\cite{LM} following very closely the thesis by
{Francesco Komauli}~\citep{komauli2018property} who has carried
out an extensive study of the list-machine in F$\#$ using the library
FsCheck.  Compared to that, we had to work harder to accomplish in Coq
what can be done fairly easily in F\#, since the latter does not have
all the constrains that Coq has as a pure functional programming
language (all functions are terminating, no side effects allowed
etc.).  We could not leverage, as we hoped, the advertised capability of 
QuickChick to automatically derive a
constrained generator from the inductive predicates of the list-machine corresponding to the preconditions of the theorems and we had to resort to a purely functional implementation.

Finally, we have carried out some mutation analysis to evaluate our
test suite and it resulted in a mutation score  comparable to
what Komauli achieved via FsCheck.

\section{Conclusions and future work}
In conclusion, QuickChick, compared to FsCheck and similar tools,
requires a steeper learning curve.  This effort is rewarded when it is
possible to automatically derive a generator from an inductive
predicate since this speeds up the whole testing process, but
currently this is possible only when the types of the parameters of
the inductive predicate are not parameterized.  It would be
interesting to retry to automatically derive generators from the
inductive relations of the list-machine when QuickChick will be able
to handle it.

We have added mutations manually; to further evaluate the
effectiveness of the properties to find errors, a possible addition of
QuickChick would be to generate valid mutants automatically.


%\newpage\cleardoublepage
\renewcommand\refname{Main References}
\nocite{Foundation,
 claessen2011quickcheck,SF,GG,LM,KO, pierce2002types,komauli2018property,Pezze}
\bibliography{Literature}\newpage\cleardoublepage

\bibliographystyle{plain}



\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

%  LocalWords:  QuickChick