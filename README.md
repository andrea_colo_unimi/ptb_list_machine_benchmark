Full paper here: https://www.researchgate.net/publication/342991295_PROPERTY-BASED_TESTING_OF_THE_LIST-MACHINE_BENCHMARK_WITH_QUICKCHICK

Using the testing library QuickChick(a clone of QuickCheck for Coq https://github.com/QuickChick/QuickChick):
1)We have tested several theorems of the Typed Arithmetic Expressions language presented 
in Software foundations, chapter Type System:
https://softwarefoundations.cis.upenn.edu/current/plf-current/index.html

2)We have tested several theorems of the List-machine benchmark
https://www.cs.princeton.edu/~appel/papers/listmach.pdf by Andrew W. Appel, Robert Dockins, and Xavier Leroy.
Please see:
http://www.cs.princeton.edu/~appel/listmachine/
http://gallium.inria.fr/~xleroy/listmachine/

Whether you modify/present this work,
please provide a written/oral mention to the authors Andrea Colò and Alberto Momigliano.

