Require Import Arith Bool List ZArith. Import ListNotations.
Require Import Coq.Lists.List.
From QuickChick Require Import QuickChick.
Import QcNotation. (* Suppress some annoying warnings: *)
Set Warnings "-extraction-opaque-accessed,-extraction". 
 Require Import String. Local Open Scope string. 
Require Export ExtLib.Structures.Monads.
Export MonadNotation. 
Open Scope monad_scope.

From T Require Import Maps.

From T Require Import Listmachine.

From T  Require Import Testing.

Module DoNotation.
Notation "'do!' X <- A ; B" :=
  (bindGen A (fun X => B))
    (at level 200, X ident, A at level 100, B at level 200).
End DoNotation.
Import DoNotation.


(* TEST MAP *)

Definition maptest1 := ((empty _) # xH <- ty_nil).

Definition maptest2 := ((empty _) # (xI xH) <- ty_nil).

Definition maptest3 := ((empty _) # (xO xH) <- ty_nil).

Definition maptest4 := ((empty _) # (xO(xO xH)) <- ty_nil).

Definition maptest5 := ((empty _) # (xI(xO xH)) <- ty_nil).

Definition maptest6 := (((empty _) #  (xO(xI xH)) <- ty_nil) # xH <- ty_nil).

Definition maptest7 := (maptest6 # (xO xH) <- ty_nil) # (xO xH) <- ty_nil.

Example test_getKeys1: getKeys _ maptest1 = [xH].
Proof. simpl. reflexivity. Qed.

Example test_getKeys2: getKeys _ maptest2 = [xI xH].
Proof. simpl. reflexivity. Qed.

Example test_getKeys3: getKeys _ maptest3 = [xO xH].
Proof. simpl. reflexivity. Qed.

Example test_getKeys4: getKeys _((empty _) # (xO(xO xH)) <- ty_nil) = [(xO(xO xH))].
Proof. simpl. reflexivity. Qed.

Example test_getKeys5: getKeys _ maptest5 = [(xI(xO xH))].
Proof. simpl. reflexivity. Qed.

Example test_getKeys6: getKeys _ maptest6 = [xH ; xO(xI xH)].
Proof. simpl. reflexivity. Qed.


Example test_ofListToList3: 
  (ofList(toList maptest3) = maptest3).
Proof. simpl. reflexivity. Qed.


Example test_ofListToList4: 
  (ofList(toList maptest4) = maptest4).
Proof. simpl. reflexivity. Qed.

Example test_ofListToList5: 
  (ofList(toList maptest5) = maptest5).
Proof. simpl. reflexivity. Qed.

Example test_ofListToList6: 
  (ofList(toList maptest6) = maptest6).
Proof. simpl. reflexivity. Qed.

Example test_ofListToList7: 
  (ofList(toList maptest7) = maptest7).
Proof. simpl. reflexivity. Qed.

(*! Section UNIT_TESTING *)(*! extends MUTANTS *)
(*Other tests to kill mutants
  -bug6 killed by testStepFunc
  -bug1 killed by instr_cons_typecheck_instrProp
  -bug17 killed by test_typecheck_block
*)

Instance dec_optiontypecheck_instr
           (pi : program_typing) (g:env) (i:instr) 
           : Dec (typecheck_instr pi g i = None).
Proof. dec_eq. Defined.


Definition instr_cons_typecheck_instrProp := 
  let x := xH in
  let y := xO xH in
  let z := xI xH in
  let l := xH in
  let pi := Maps.ofList [(l, (Maps.ofList [(x, ty_nil)]))] in
  let g := Maps.ofList [(x, ty_nil)] in 
  let i := instr_cons x y z in 
    (typecheck_instr pi g i = None ?).

(*! QuickCheck instr_cons_typecheck_instrProp. *)

Instance dec_store_instr
          (t1 t2 : store * instr)
           : Dec (t1 = t2).
Proof. dec_eq. Defined.

Definition testStepFunc :=
    let p := fromListToProgram [] in 
    let r := Maps.ofList [] in
    let i := 
    instr_seq 
        (instr_seq (instr_jump xH)(instr_jump (xO xH)))
        (instr_jump (xI xH)) in
    match stepFunc r i p with
    |Some x => 
      x = (Maps.ofList [], 
          (instr_jump xH) :: (instr_jump (xO xH)) 
              :: (instr_jump (xI xH))) ?
    |None => false
    end.

(*! QuickCheck testStepFunc. *)

Definition test_typecheck_block :=
  let x := xH in
  let y := xO xH in
  let pi := Maps.ofList [] in
  let g := Maps.ofList [(x, ty_listcons ty_nil)] in
  let i := instr_seq (instr_fetch_field x 0 y) instr_halt in
  typecheck_block pi g i.

(*! QuickCheck test_typecheck_block. *)


